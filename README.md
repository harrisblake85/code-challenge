# Circadence Engineer Recruitment Test

## About Circadence

Circadence Corporation is the market leader in next-generation cybersecurity readiness.

Our history of software advancement, multi-player game development, and a deep understanding of application optimization allows us to help develop cyber professionals who are problem-solvers, critical thinkers, and passionate about protecting and defending their organization.

Project Ares allows cyber teams from across enterprise, government, and academic institutions to automate and augment the cyber workforce by providing immersive, gamified cyber range learning environments that emulate company networks. Realistic scenarios engage teams in immersive, mission-specific virtual environments using real-world tools, network activity and a large library of authentic threat scenarios.

## Engineering Applicant Coding Challenges

Most engineering positions require an applicant to complete a coding challenge.
The coding challenges are located at <https://gitlab.com/Circadence-Public/code-challenge.>  Each application category has a corresponding test described in a file of the following format:  

{applicationCategory}.{applicationArea}.{applicationLevel}.md

E.g., the markdown file Engineer.Backend.Intern.md contains the instructions and questions for completing a challenge
    for the **Engineer**ing team
    as a **Backend** Developer
    at the **Intern** level

Each challenge will have the following sections:

- a description of the Coding Challenge
- a description of Platform Choices
- the Task Requirements that specifies minimum requirements for completion and any optional requirements
- one or more user stories that pertain to the challenge
- acceptance criteria for the user stories
- a questionaire to be completed as part of the challenge

Email any coding challenge related questions to: eng_challenge_help@circadence.com

